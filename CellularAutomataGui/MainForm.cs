﻿using System;
using System.Windows.Forms;
using CellularAutomata.Core.MonteCarloModels;
using CellularAutomata.Core;
using CellularAutomata.Core.GrainGrowth;
using CellularAutomata.Core.Initializers;
using CellularAutomata.Core.NeighbourhoodStrategies;
using CellularAutomata.Core.NucleationChangeStrategies;
using CellularAutomata.Core.Simulations;
using CellularAutomataGui.Drawers;

namespace CellularAutomataGui
{
	public partial class MainForm : Form
	{
		private MonteCarloWorld _world;
		private MonteCarloWorld _worldCopy;
		private readonly IMonteCarloWorldInitializer _worldInitializer;
		private readonly IMonteCarloWorldFactory _worldFactory;
		private readonly MonteCarloWorldDrawer _monteCarloWorldDrawer;
		private readonly MonteCarloEnergyDrawer _energyDrawer;
		private readonly GrainGrower _grainGrower;
		private const int WorldSize = 60;
		private const double WorldInitialCoverage = 0.01;

		public MainForm()
		{
			InitializeComponent();
			_worldFactory = new MonteCarloWorldFactory();
			_monteCarloWorldDrawer = new MonteCarloWorldDrawer(BoardPanel);
			_energyDrawer = new MonteCarloEnergyDrawer(BoardPanel);
			_worldInitializer = new WorldInitializer();
			_grainGrower = new GrainGrower(new MooreNeighbourhoodStrategy());
			ResetWorld();
		}

		#region GUI handlers
		private void GenerateInitialStateButton_Click(object sender, EventArgs e)
		{
			ResetWorld();
			_worldInitializer.InitializeWorld(_world, coverage: WorldInitialCoverage);
			
			while(true)
			{
				var hasChanges = _grainGrower.PerformGrainGrowthStep(_world);
				if (!hasChanges)
					break;
			}
			_monteCarloWorldDrawer.DrawWorld();
			_worldCopy = new MonteCarloWorld(_world);
		}

		private void RunSrxmcButton_Click(object sender, EventArgs e)
		{
			var simulation = BuildSimulation();
			simulation.InitializeCellsEnergy();
			while (!simulation.IsComplete)
			{
				simulation.AddNucleons();
				simulation.RecrystallizationStep();
				_monteCarloWorldDrawer.DrawWorld();
			}
		}

		private void VisualizeEnergyButton_Click(object sender, EventArgs e)
		{
			_energyDrawer.DrawWorld();
		}

		private void VisualizeInitialWorldButton_Click(object sender, EventArgs e)
		{
			_monteCarloWorldDrawer.AttachWorld(_worldCopy);
			_monteCarloWorldDrawer.DrawWorld();
		}

		private void VisualizeRecrystallizedWorldButton_Click(object sender, EventArgs e)
		{
			_monteCarloWorldDrawer.AttachWorld(_world);
			_monteCarloWorldDrawer.DrawWorld();
		}
		#endregion

		private void ResetWorld()
		{
			_world = _worldFactory.Create(WorldSize);
			_monteCarloWorldDrawer.AttachWorld(_world);
			_energyDrawer.AttachWorld(_world);
		}

		private INucleationChangeStrategy GetNucleationChangeStrategy()
		{
			if(NucleationTypeConstantRadioButton.Checked)
				return new ConstantNucleationChangeStrategy();
			if (NucleationTypeIncreasingRadioButton.Checked)
			{
				var increment = (int) NucleationIncrementNUD.Value;
				return new IncreasingNucleationChangeStrategy(increment);
			}
			if (NucleationTypeDecreasingRadioButton.Checked)
			{
				var decrement = (int) NucleationDecrementNUD.Value;
				return new DecreasingNucleationChangeStrategy(decrement);
			}
			if (NucleationTypeAtTheBegginingOfSimulationRadioButton.Checked)
				return new SingleFeedNucleationChangeStrategy();

			throw new Exception("Nucleation change strategy cannot be resolved.");
		}

		private NucleationPropagationType GetNucleationPropagationType()
		{
			return HomogenousPropagationRadioButton.Checked
				? NucleationPropagationType.Homogenous
				: NucleationPropagationType.Heterogenous;
		}

		private SrxmcSimulation BuildSimulation()
		{
			var stepsLimit = (int)StepsNUD.Value;
			var hParam = (int)HParamNud.Value;
			var addNucleonsOnBordersOnly = AddNucleonsOnBordersOnlyRadioButton.Checked;
			var nucleationConfiguration = new NucleationConfiguration
			{
				NucleationChangeStrategy = GetNucleationChangeStrategy(),
				NucleationPropagationType = GetNucleationPropagationType(),
				AddNucleonsOnlyOnBorders = addNucleonsOnBordersOnly
			};
			return new SrxmcSimulation(_world, nucleationConfiguration, stepsLimit, hParam, initialNucleonsAmount: 10);
		}

		
	}
}
