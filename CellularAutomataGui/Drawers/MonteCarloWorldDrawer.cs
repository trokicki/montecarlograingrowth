using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using CellularAutomata.Core.Models;
using CellularAutomata.Core.MonteCarloModels;

namespace CellularAutomataGui.Drawers
{
	internal class MonteCarloWorldDrawer : IDisposable
	{
		private readonly IDictionary<int, Brush> _brushes;
		protected readonly Control Control;
		protected readonly IDictionary<Cell, Coords2D> CellsCoords;
		protected MonteCarloWorld World;
		protected int CellSize;

		public MonteCarloWorldDrawer(Control control)
		{
			Control = control;
			_brushes = new Dictionary<int, Brush>();
			CellsCoords = new Dictionary<Cell, Coords2D>();
		}

		public void AttachWorld(MonteCarloWorld world)
		{
			World = world;
			CellSize = GetCellSize();
			InitCellsCoords();
		}

		public virtual void DrawWorld()
		{
			const int padding = 0;
			DefineBrushes();
			using (var graphics = Control.CreateGraphics())
			{
				foreach (var cell in World.Cells)
				{
					var cellCoords = CellsCoords[cell];
					var brush = _brushes[cell.StateNumber];
					var cellRectangle = new Rectangle(
						cellCoords.X + padding,
						cellCoords.Y + padding,
						CellSize - padding * 2,
						CellSize - padding * 2);
					graphics.FillRectangle(brush, cellRectangle);
				}
			}
		}

		private int GetCellSize()
		{
			var controlSmallerSize = Math.Min(Control.Width, Control.Height);
			var worldBiggerSize = Math.Max(World.Cells.GetLength(0), World.Cells.GetLength(1));
			return controlSmallerSize / worldBiggerSize;
		}

		private void DefineBrushes()
		{
			foreach (var cell in World.Cells.Cast<MonteCarloCell>().Where(cell => !_brushes.ContainsKey(cell.StateNumber)))
			{
				_brushes[cell.StateNumber] = new SolidBrush(GenerateColor(cell.StateNumber, cell.IsRecrystallized));
			}
		}

		private void InitCellsCoords()
		{
			for (var i = 0; i < World.Size * World.Size; i++)
			{
				var x = i / World.Size;
				var y = i % World.Size;
				var cell = World.Cells[x, y];
				CellsCoords[cell] = new Coords2D { X = x * CellSize, Y = y * CellSize };
			}
		}

		private static Color GenerateColor(int stateNumber, bool isRecrystallized)
		{
			return isRecrystallized
				? Color.FromArgb(stateNumber * 70 % 150, stateNumber * 75 % 150, stateNumber * 90 % 100)
				: Color.FromArgb(stateNumber * 70 % 150 + 100, stateNumber * 75 % 150 + 100, stateNumber * 90 % 120 + 130);
		}

		#region IDisposable

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (!disposing)
				return;

			foreach (var brush in _brushes)
				brush.Value?.Dispose();
		}

		~MonteCarloWorldDrawer()
		{
			Dispose(false);
		}

		#endregion
	}
}