using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using CellularAutomata.Core.MonteCarloModels;

namespace CellularAutomataGui.Drawers
{
	internal class MonteCarloEnergyDrawer : MonteCarloWorldDrawer
	{
		public MonteCarloEnergyDrawer(Control control) : base(control)
		{
		}

		public override void DrawWorld()
		{
			var maximumEnergy = World.Cells.Cast<MonteCarloCell>().Max(c => c.Energy);
			var minimumEnergy = World.Cells.Cast<MonteCarloCell>().Min(c => c.Energy);
			using (var graphics = Control.CreateGraphics())
			{
				foreach (var cell in World.Cells)
				{
					var cellCoords = CellsCoords[cell];
					var cellColor = CalculateEnergyColor(cell.Energy, maximumEnergy, minimumEnergy);
					using (var brush = new SolidBrush(cellColor))
					{
						var cellRectangle = new Rectangle(
							cellCoords.X, cellCoords.Y,
							CellSize, CellSize);
						graphics.FillRectangle(brush, cellRectangle);
					}
				}
			}
		}

		private Color CalculateEnergyColor(int energy, int maximumEnergy, int minimumEnergy)
		{
			var energyDelta = maximumEnergy - minimumEnergy;
			if(energyDelta <= 0)
				return Color.Black;
			var percentage = (double)(energy - minimumEnergy) / (maximumEnergy - minimumEnergy);
			return Color.FromArgb(255, (int)((1.0 - percentage) * 255), 0);
		}
	}
}