﻿namespace CellularAutomataGui
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.BoardPanel = new System.Windows.Forms.Panel();
			this.StepsNUD = new System.Windows.Forms.NumericUpDown();
			this.label1 = new System.Windows.Forms.Label();
			this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
			this.label2 = new System.Windows.Forms.Label();
			this.NucleationTypeConstantRadioButton = new System.Windows.Forms.RadioButton();
			this.NucleationTypeIncreasingRadioButton = new System.Windows.Forms.RadioButton();
			this.NucleationIncrementNUD = new System.Windows.Forms.NumericUpDown();
			this.NucleationTypeDecreasingRadioButton = new System.Windows.Forms.RadioButton();
			this.NucleationDecrementNUD = new System.Windows.Forms.NumericUpDown();
			this.NucleationTypeAtTheBegginingOfSimulationRadioButton = new System.Windows.Forms.RadioButton();
			this.label3 = new System.Windows.Forms.Label();
			this.HParamNud = new System.Windows.Forms.NumericUpDown();
			this.GenerateInitialStateButton = new System.Windows.Forms.Button();
			this.RunSrxmcButton = new System.Windows.Forms.Button();
			this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
			this.label5 = new System.Windows.Forms.Label();
			this.HomogenousPropagationRadioButton = new System.Windows.Forms.RadioButton();
			this.HeterogenousPropagationRadioButton = new System.Windows.Forms.RadioButton();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.label4 = new System.Windows.Forms.Label();
			this.HBoundaryParamNUD = new System.Windows.Forms.NumericUpDown();
			this.VisualizeEnergyButton = new System.Windows.Forms.Button();
			this.VisualizeInitialWorldButton = new System.Windows.Forms.Button();
			this.VisualizeRecrystallizedWorldButton = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.panel2 = new System.Windows.Forms.Panel();
			this.AddNucleonsAnywhereRadioButton = new System.Windows.Forms.RadioButton();
			this.AddNucleonsOnBordersOnlyRadioButton = new System.Windows.Forms.RadioButton();
			((System.ComponentModel.ISupportInitialize)(this.StepsNUD)).BeginInit();
			this.flowLayoutPanel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.NucleationIncrementNUD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.NucleationDecrementNUD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.HParamNud)).BeginInit();
			this.flowLayoutPanel2.SuspendLayout();
			this.tableLayoutPanel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.HBoundaryParamNUD)).BeginInit();
			this.groupBox1.SuspendLayout();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// BoardPanel
			// 
			this.BoardPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.BoardPanel.BackColor = System.Drawing.Color.Azure;
			this.BoardPanel.Location = new System.Drawing.Point(205, 5);
			this.BoardPanel.Name = "BoardPanel";
			this.BoardPanel.Size = new System.Drawing.Size(666, 670);
			this.BoardPanel.TabIndex = 0;
			// 
			// StepsNUD
			// 
			this.StepsNUD.Location = new System.Drawing.Point(71, 3);
			this.StepsNUD.Name = "StepsNUD";
			this.StepsNUD.Size = new System.Drawing.Size(46, 20);
			this.StepsNUD.TabIndex = 1;
			this.StepsNUD.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(3, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(34, 13);
			this.label1.TabIndex = 2;
			this.label1.Text = "Steps";
			// 
			// flowLayoutPanel1
			// 
			this.flowLayoutPanel1.Controls.Add(this.label2);
			this.flowLayoutPanel1.Controls.Add(this.NucleationTypeConstantRadioButton);
			this.flowLayoutPanel1.Controls.Add(this.NucleationTypeIncreasingRadioButton);
			this.flowLayoutPanel1.Controls.Add(this.NucleationIncrementNUD);
			this.flowLayoutPanel1.Controls.Add(this.NucleationTypeDecreasingRadioButton);
			this.flowLayoutPanel1.Controls.Add(this.NucleationDecrementNUD);
			this.flowLayoutPanel1.Controls.Add(this.NucleationTypeAtTheBegginingOfSimulationRadioButton);
			this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
			this.flowLayoutPanel1.Location = new System.Drawing.Point(12, 108);
			this.flowLayoutPanel1.Name = "flowLayoutPanel1";
			this.flowLayoutPanel1.Size = new System.Drawing.Size(182, 165);
			this.flowLayoutPanel1.TabIndex = 3;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(3, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(81, 13);
			this.label2.TabIndex = 4;
			this.label2.Text = "Nucleation type";
			// 
			// NucleationTypeConstantRadioButton
			// 
			this.NucleationTypeConstantRadioButton.AutoSize = true;
			this.NucleationTypeConstantRadioButton.Checked = true;
			this.NucleationTypeConstantRadioButton.Location = new System.Drawing.Point(3, 16);
			this.NucleationTypeConstantRadioButton.Name = "NucleationTypeConstantRadioButton";
			this.NucleationTypeConstantRadioButton.Size = new System.Drawing.Size(67, 17);
			this.NucleationTypeConstantRadioButton.TabIndex = 0;
			this.NucleationTypeConstantRadioButton.TabStop = true;
			this.NucleationTypeConstantRadioButton.Text = "Constant";
			this.NucleationTypeConstantRadioButton.UseVisualStyleBackColor = true;
			// 
			// NucleationTypeIncreasingRadioButton
			// 
			this.NucleationTypeIncreasingRadioButton.AutoSize = true;
			this.NucleationTypeIncreasingRadioButton.Location = new System.Drawing.Point(3, 39);
			this.NucleationTypeIncreasingRadioButton.Name = "NucleationTypeIncreasingRadioButton";
			this.NucleationTypeIncreasingRadioButton.Size = new System.Drawing.Size(74, 17);
			this.NucleationTypeIncreasingRadioButton.TabIndex = 1;
			this.NucleationTypeIncreasingRadioButton.Text = "Increasing";
			this.NucleationTypeIncreasingRadioButton.UseVisualStyleBackColor = true;
			// 
			// NucleationIncrementNUD
			// 
			this.NucleationIncrementNUD.Location = new System.Drawing.Point(3, 62);
			this.NucleationIncrementNUD.Name = "NucleationIncrementNUD";
			this.NucleationIncrementNUD.Size = new System.Drawing.Size(46, 20);
			this.NucleationIncrementNUD.TabIndex = 12;
			this.NucleationIncrementNUD.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
			// 
			// NucleationTypeDecreasingRadioButton
			// 
			this.NucleationTypeDecreasingRadioButton.AutoSize = true;
			this.NucleationTypeDecreasingRadioButton.Location = new System.Drawing.Point(3, 88);
			this.NucleationTypeDecreasingRadioButton.Name = "NucleationTypeDecreasingRadioButton";
			this.NucleationTypeDecreasingRadioButton.Size = new System.Drawing.Size(79, 17);
			this.NucleationTypeDecreasingRadioButton.TabIndex = 2;
			this.NucleationTypeDecreasingRadioButton.Text = "Decreasing";
			this.NucleationTypeDecreasingRadioButton.UseVisualStyleBackColor = true;
			// 
			// NucleationDecrementNUD
			// 
			this.NucleationDecrementNUD.Location = new System.Drawing.Point(3, 111);
			this.NucleationDecrementNUD.Name = "NucleationDecrementNUD";
			this.NucleationDecrementNUD.Size = new System.Drawing.Size(46, 20);
			this.NucleationDecrementNUD.TabIndex = 13;
			this.NucleationDecrementNUD.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
			// 
			// NucleationTypeAtTheBegginingOfSimulationRadioButton
			// 
			this.NucleationTypeAtTheBegginingOfSimulationRadioButton.AutoSize = true;
			this.NucleationTypeAtTheBegginingOfSimulationRadioButton.Location = new System.Drawing.Point(3, 137);
			this.NucleationTypeAtTheBegginingOfSimulationRadioButton.Name = "NucleationTypeAtTheBegginingOfSimulationRadioButton";
			this.NucleationTypeAtTheBegginingOfSimulationRadioButton.Size = new System.Drawing.Size(163, 17);
			this.NucleationTypeAtTheBegginingOfSimulationRadioButton.TabIndex = 3;
			this.NucleationTypeAtTheBegginingOfSimulationRadioButton.Text = "At the beggining of simulation";
			this.NucleationTypeAtTheBegginingOfSimulationRadioButton.UseVisualStyleBackColor = true;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(3, 26);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(49, 13);
			this.label3.TabIndex = 6;
			this.label3.Text = "H normal";
			// 
			// HParamNud
			// 
			this.HParamNud.Location = new System.Drawing.Point(71, 29);
			this.HParamNud.Name = "HParamNud";
			this.HParamNud.Size = new System.Drawing.Size(39, 20);
			this.HParamNud.TabIndex = 5;
			this.HParamNud.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
			// 
			// GenerateInitialStateButton
			// 
			this.GenerateInitialStateButton.Location = new System.Drawing.Point(9, 462);
			this.GenerateInitialStateButton.Name = "GenerateInitialStateButton";
			this.GenerateInitialStateButton.Size = new System.Drawing.Size(185, 23);
			this.GenerateInitialStateButton.TabIndex = 9;
			this.GenerateInitialStateButton.Text = "Initialize world";
			this.GenerateInitialStateButton.UseVisualStyleBackColor = true;
			this.GenerateInitialStateButton.Click += new System.EventHandler(this.GenerateInitialStateButton_Click);
			// 
			// RunSrxmcButton
			// 
			this.RunSrxmcButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.RunSrxmcButton.Location = new System.Drawing.Point(9, 491);
			this.RunSrxmcButton.Name = "RunSrxmcButton";
			this.RunSrxmcButton.Size = new System.Drawing.Size(185, 31);
			this.RunSrxmcButton.TabIndex = 10;
			this.RunSrxmcButton.Text = "Run SRXMC";
			this.RunSrxmcButton.UseVisualStyleBackColor = true;
			this.RunSrxmcButton.Click += new System.EventHandler(this.RunSrxmcButton_Click);
			// 
			// flowLayoutPanel2
			// 
			this.flowLayoutPanel2.Controls.Add(this.label5);
			this.flowLayoutPanel2.Controls.Add(this.panel2);
			this.flowLayoutPanel2.Controls.Add(this.panel1);
			this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
			this.flowLayoutPanel2.Location = new System.Drawing.Point(12, 286);
			this.flowLayoutPanel2.Name = "flowLayoutPanel2";
			this.flowLayoutPanel2.Size = new System.Drawing.Size(182, 126);
			this.flowLayoutPanel2.TabIndex = 5;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(3, 0);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(117, 13);
			this.label5.TabIndex = 4;
			this.label5.Text = "Nucleation propagation";
			// 
			// HomogenousPropagationRadioButton
			// 
			this.HomogenousPropagationRadioButton.AutoSize = true;
			this.HomogenousPropagationRadioButton.Checked = true;
			this.HomogenousPropagationRadioButton.Location = new System.Drawing.Point(3, 3);
			this.HomogenousPropagationRadioButton.Name = "HomogenousPropagationRadioButton";
			this.HomogenousPropagationRadioButton.Size = new System.Drawing.Size(88, 17);
			this.HomogenousPropagationRadioButton.TabIndex = 0;
			this.HomogenousPropagationRadioButton.TabStop = true;
			this.HomogenousPropagationRadioButton.Text = "Homogenous";
			this.HomogenousPropagationRadioButton.UseVisualStyleBackColor = true;
			// 
			// HeterogenousPropagationRadioButton
			// 
			this.HeterogenousPropagationRadioButton.AutoSize = true;
			this.HeterogenousPropagationRadioButton.Location = new System.Drawing.Point(3, 26);
			this.HeterogenousPropagationRadioButton.Name = "HeterogenousPropagationRadioButton";
			this.HeterogenousPropagationRadioButton.Size = new System.Drawing.Size(92, 17);
			this.HeterogenousPropagationRadioButton.TabIndex = 1;
			this.HeterogenousPropagationRadioButton.Text = "Heterogenous";
			this.HeterogenousPropagationRadioButton.UseVisualStyleBackColor = true;
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 2;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.StepsNUD, 1, 0);
			this.tableLayoutPanel1.Controls.Add(this.HParamNud, 1, 1);
			this.tableLayoutPanel1.Controls.Add(this.label3, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.label4, 0, 2);
			this.tableLayoutPanel1.Controls.Add(this.HBoundaryParamNUD, 1, 2);
			this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 3;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.Size = new System.Drawing.Size(182, 79);
			this.tableLayoutPanel1.TabIndex = 11;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(3, 52);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(62, 13);
			this.label4.TabIndex = 7;
			this.label4.Text = "H boundary";
			// 
			// HBoundaryParamNUD
			// 
			this.HBoundaryParamNUD.Location = new System.Drawing.Point(71, 55);
			this.HBoundaryParamNUD.Name = "HBoundaryParamNUD";
			this.HBoundaryParamNUD.Size = new System.Drawing.Size(39, 20);
			this.HBoundaryParamNUD.TabIndex = 8;
			this.HBoundaryParamNUD.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
			// 
			// VisualizeEnergyButton
			// 
			this.VisualizeEnergyButton.Location = new System.Drawing.Point(26, 19);
			this.VisualizeEnergyButton.Name = "VisualizeEnergyButton";
			this.VisualizeEnergyButton.Size = new System.Drawing.Size(123, 23);
			this.VisualizeEnergyButton.TabIndex = 12;
			this.VisualizeEnergyButton.Text = "Energy";
			this.VisualizeEnergyButton.UseVisualStyleBackColor = true;
			this.VisualizeEnergyButton.Click += new System.EventHandler(this.VisualizeEnergyButton_Click);
			// 
			// VisualizeInitialWorldButton
			// 
			this.VisualizeInitialWorldButton.Location = new System.Drawing.Point(26, 48);
			this.VisualizeInitialWorldButton.Name = "VisualizeInitialWorldButton";
			this.VisualizeInitialWorldButton.Size = new System.Drawing.Size(123, 23);
			this.VisualizeInitialWorldButton.TabIndex = 13;
			this.VisualizeInitialWorldButton.Text = "Initial world";
			this.VisualizeInitialWorldButton.UseVisualStyleBackColor = true;
			this.VisualizeInitialWorldButton.Click += new System.EventHandler(this.VisualizeInitialWorldButton_Click);
			// 
			// VisualizeRecrystallizedWorldButton
			// 
			this.VisualizeRecrystallizedWorldButton.Location = new System.Drawing.Point(26, 77);
			this.VisualizeRecrystallizedWorldButton.Name = "VisualizeRecrystallizedWorldButton";
			this.VisualizeRecrystallizedWorldButton.Size = new System.Drawing.Size(123, 23);
			this.VisualizeRecrystallizedWorldButton.TabIndex = 14;
			this.VisualizeRecrystallizedWorldButton.Text = "Recrystallized world";
			this.VisualizeRecrystallizedWorldButton.UseVisualStyleBackColor = true;
			this.VisualizeRecrystallizedWorldButton.Click += new System.EventHandler(this.VisualizeRecrystallizedWorldButton_Click);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.VisualizeEnergyButton);
			this.groupBox1.Controls.Add(this.VisualizeRecrystallizedWorldButton);
			this.groupBox1.Controls.Add(this.VisualizeInitialWorldButton);
			this.groupBox1.Location = new System.Drawing.Point(9, 540);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(185, 109);
			this.groupBox1.TabIndex = 15;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Visualization";
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.AddNucleonsAnywhereRadioButton);
			this.panel1.Controls.Add(this.AddNucleonsOnBordersOnlyRadioButton);
			this.panel1.Location = new System.Drawing.Point(3, 75);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(179, 48);
			this.panel1.TabIndex = 5;
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.HomogenousPropagationRadioButton);
			this.panel2.Controls.Add(this.HeterogenousPropagationRadioButton);
			this.panel2.Location = new System.Drawing.Point(3, 16);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(179, 53);
			this.panel2.TabIndex = 0;
			// 
			// AddNucleonsAnywhereRadioButton
			// 
			this.AddNucleonsAnywhereRadioButton.AutoSize = true;
			this.AddNucleonsAnywhereRadioButton.Checked = true;
			this.AddNucleonsAnywhereRadioButton.Location = new System.Drawing.Point(3, 3);
			this.AddNucleonsAnywhereRadioButton.Name = "AddNucleonsAnywhereRadioButton";
			this.AddNucleonsAnywhereRadioButton.Size = new System.Drawing.Size(72, 17);
			this.AddNucleonsAnywhereRadioButton.TabIndex = 2;
			this.AddNucleonsAnywhereRadioButton.TabStop = true;
			this.AddNucleonsAnywhereRadioButton.Text = "Anywhere";
			this.AddNucleonsAnywhereRadioButton.UseVisualStyleBackColor = true;
			// 
			// AddNucleonsOnBordersOnlyRadioButton
			// 
			this.AddNucleonsOnBordersOnlyRadioButton.AutoSize = true;
			this.AddNucleonsOnBordersOnlyRadioButton.Location = new System.Drawing.Point(3, 26);
			this.AddNucleonsOnBordersOnlyRadioButton.Name = "AddNucleonsOnBordersOnlyRadioButton";
			this.AddNucleonsOnBordersOnlyRadioButton.Size = new System.Drawing.Size(83, 17);
			this.AddNucleonsOnBordersOnlyRadioButton.TabIndex = 3;
			this.AddNucleonsOnBordersOnlyRadioButton.Text = "Borders only";
			this.AddNucleonsOnBordersOnlyRadioButton.UseVisualStyleBackColor = true;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(878, 681);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.tableLayoutPanel1);
			this.Controls.Add(this.RunSrxmcButton);
			this.Controls.Add(this.GenerateInitialStateButton);
			this.Controls.Add(this.flowLayoutPanel2);
			this.Controls.Add(this.flowLayoutPanel1);
			this.Controls.Add(this.BoardPanel);
			this.Name = "MainForm";
			this.Text = "Main";
			((System.ComponentModel.ISupportInitialize)(this.StepsNUD)).EndInit();
			this.flowLayoutPanel1.ResumeLayout(false);
			this.flowLayoutPanel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.NucleationIncrementNUD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.NucleationDecrementNUD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.HParamNud)).EndInit();
			this.flowLayoutPanel2.ResumeLayout(false);
			this.flowLayoutPanel2.PerformLayout();
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.HBoundaryParamNUD)).EndInit();
			this.groupBox1.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel BoardPanel;
		private System.Windows.Forms.NumericUpDown StepsNUD;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
		private System.Windows.Forms.RadioButton NucleationTypeConstantRadioButton;
		private System.Windows.Forms.RadioButton NucleationTypeIncreasingRadioButton;
		private System.Windows.Forms.RadioButton NucleationTypeDecreasingRadioButton;
		private System.Windows.Forms.RadioButton NucleationTypeAtTheBegginingOfSimulationRadioButton;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.NumericUpDown HParamNud;
		private System.Windows.Forms.Button GenerateInitialStateButton;
		private System.Windows.Forms.Button RunSrxmcButton;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.RadioButton HomogenousPropagationRadioButton;
		private System.Windows.Forms.RadioButton HeterogenousPropagationRadioButton;
		private System.Windows.Forms.NumericUpDown NucleationIncrementNUD;
		private System.Windows.Forms.NumericUpDown NucleationDecrementNUD;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.NumericUpDown HBoundaryParamNUD;
		private System.Windows.Forms.Button VisualizeEnergyButton;
		private System.Windows.Forms.Button VisualizeInitialWorldButton;
		private System.Windows.Forms.Button VisualizeRecrystallizedWorldButton;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.RadioButton AddNucleonsAnywhereRadioButton;
		private System.Windows.Forms.RadioButton AddNucleonsOnBordersOnlyRadioButton;
	}
}

