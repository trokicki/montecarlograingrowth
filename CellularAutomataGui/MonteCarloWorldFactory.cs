using CellularAutomata.Core.MonteCarloModels;

namespace CellularAutomataGui
{
	public class MonteCarloWorldFactory : IMonteCarloWorldFactory
	{
		public MonteCarloWorld Create(int size)
		{
			return new MonteCarloWorld(size);
		}
	}
}