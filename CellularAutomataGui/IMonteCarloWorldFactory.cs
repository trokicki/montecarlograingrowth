﻿using CellularAutomata.Core.MonteCarloModels;

namespace CellularAutomataGui
{
	public interface IMonteCarloWorldFactory
	{
		MonteCarloWorld Create(int size);
	}
}