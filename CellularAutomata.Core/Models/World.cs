﻿namespace CellularAutomata.Core.Models
{
	public class World<T> where T : Cell, new()
	{
		public T[,] Cells { get; set; }
		public int Size { get; }

		public World(int size)
		{
			Size = size;
			Cells = new T[size, size];
			for (var i = 0; i < Size * Size; i++)
			{
				Cells[i / Size, i % Size] = new T
				{
					X = i / Size,
					Y = i % Size
				};
			}
		}

		public World(World<T> world)
		{
			Size = world.Size;
			Cells = new T[Size, Size];
			for (var i = 0; i < Size * Size; i++)
			{
				Cells[i / Size, i % Size] = (T)world.Cells[i / Size, i % Size].Clone();
			}
		}
	}
}
