using System;

namespace CellularAutomata.Core.Models
{
	public interface ICell
	{
		 int X { get; set; }
		 int Y { get; set; }
	}

	public interface IDrawableCell : ICell { }

	public class Cell : ICell
	{
		public int X { get; set; }
		public int Y { get; set; }

		public Cell()
		{

		}

		public Cell(int x, int y)
		{
			X = x;
			Y = y;
		}

		public virtual Cell Clone()
		{
			return new Cell(X, Y);
		}
	}
}
