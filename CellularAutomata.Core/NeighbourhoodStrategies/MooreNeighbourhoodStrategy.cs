using System.Collections.Generic;
using CellularAutomata.Core.Models;
using CellularAutomata.Core.MonteCarloModels;

namespace CellularAutomata.Core.NeighbourhoodStrategies
{
	public class MooreNeighbourhoodStrategy : INeighbourhoodStrategy
	{
		public IEnumerable<MonteCarloCell> GetNeighbours(ICell cell, World<MonteCarloCell> world)
		{
			if (cell.Y > 0)
			{
				if (cell.X > 0)
					yield return world.Cells[cell.X - 1, cell.Y - 1];
				yield return world.Cells[cell.X, cell.Y - 1];
				if (cell.X < world.Cells.GetUpperBound(0))
					yield return world.Cells[cell.X + 1, cell.Y - 1];
			}
			if (cell.X > 0)
				yield return world.Cells[cell.X - 1, cell.Y];

			if (cell.X < world.Cells.GetUpperBound(0))
				yield return world.Cells[cell.X + 1, cell.Y];

			if (cell.Y < world.Cells.GetUpperBound(1))
			{
				if (cell.X > 0)
					yield return world.Cells[cell.X - 1, cell.Y + 1];
				yield return world.Cells[cell.X, cell.Y + 1];
				if (cell.X < world.Cells.GetUpperBound(0))
					yield return world.Cells[cell.X + 1, cell.Y + 1];
			}
		}
	}
}