using System;
using System.Linq;
using CellularAutomata.Core.Models;
using CellularAutomata.Core.MonteCarloModels;

namespace CellularAutomata.Core.Initializers
{
	public class WorldInitializer : IMonteCarloWorldInitializer
	{
		private readonly Random _random = new Random();

		public void InitializeWorld(World<MonteCarloCell> world, double coverage)
		{
			var cellsAmount = (int)(world.Cells.Length * coverage);
			var pairs = Enumerable.Range(0, cellsAmount).Select(x => new[] { _random.Next(world.Size), _random.Next(world.Size) });

			foreach (var cell in world.Cells)
				cell.StateNumber = 0;

			var stateNumber = 1;
			foreach (var coordinates in pairs)
			{
				world.Cells[coordinates[0], coordinates[1]].StateNumber = stateNumber;
				stateNumber++;
			}
		}
	}
}