using CellularAutomata.Core.MonteCarloModels;

namespace CellularAutomata.Core.Factories
{
	public class MonteCarloWorldFactory : IMonteCarloWorldFactory
	{
		public MonteCarloWorld Create(int size)
		{
			return new MonteCarloWorld(size);
		}
	}
}