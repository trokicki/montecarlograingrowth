﻿using CellularAutomata.Core.Models;

namespace CellularAutomata.Core.MonteCarloModels
{
	public class MonteCarloCell : Cell
	{
		public int StateNumber { get; set; }
		public int NextStateNumber { get; set; }
		public int Energy { get; set; }
		public bool IsRecrystallized { get; set; }

		public override string ToString()
		{
			return string.Format("[{0}, {1}] Energy={2}, Recrystallized={3}, State={4}, NextState={5}", 
				X, Y, Energy, IsRecrystallized, StateNumber, NextStateNumber);
		}

		public override Cell Clone()
		{
			return new MonteCarloCell
			{
				X = X,
				Y = Y,
				StateNumber = StateNumber,
				Energy = Energy,
				IsRecrystallized = IsRecrystallized,
				NextStateNumber = NextStateNumber
			};
		}
	}
}
