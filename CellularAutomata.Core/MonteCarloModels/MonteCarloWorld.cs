using System;
using System.Collections.Generic;
using System.Linq;
using CellularAutomata.Core.Models;

namespace CellularAutomata.Core.MonteCarloModels
{
	public interface ICloneable<T>
	{
		T Clone();
	}

	public class MonteCarloWorld : World<MonteCarloCell>, ICloneable<MonteCarloWorld>
	{
		//TODO: optimize (cache)
		public int HighestCellState { get { return Cells.Cast<MonteCarloCell>().Max(c => c.StateNumber); } }

		public MonteCarloWorld(int size) : base(size)
		{
		}

		public MonteCarloWorld(World<MonteCarloCell> world) : base(world)
		{
		}

		public MonteCarloWorld Clone()
		{
			return new MonteCarloWorld(this);
		}
	}
}