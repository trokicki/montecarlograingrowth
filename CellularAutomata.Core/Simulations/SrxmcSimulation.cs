using System;
using System.Collections.Generic;
using System.Linq;
using CellularAutomata.Core.Extensions;
using CellularAutomata.Core.Models;
using CellularAutomata.Core.MonteCarloModels;
using CellularAutomata.Core.NeighbourhoodStrategies;

namespace CellularAutomata.Core.Simulations
{
	public enum NucleationPropagationType
	{
		Homogenous, Heterogenous
	}

	public class NucleationConfiguration
	{
		public INucleationChangeStrategy NucleationChangeStrategy;
		public NucleationPropagationType NucleationPropagationType;
		public bool AddNucleonsOnlyOnBorders { get; set; }
	}

	public class SrxmcSimulation
	{
		public bool IsComplete { get { return _step >= _stepsLimit; } }

		private MonteCarloWorld World { get; }
		private readonly bool _addNucleonsOnBordersOnly;
		private readonly NucleationPropagationType _nucleationPropagationType;
		private readonly INucleationChangeStrategy _nucleationChangeStrategy;
		private readonly INeighbourhoodStrategy _allNeighboursStrategy;
		private const int JParam = 1;
		private readonly int _hParam;
		private readonly int _stepsLimit;
		private int _step;
		private int _newNucleonsPerStep;
		private const int InitialCellEnergy = 10;
		private const int InitialCellHeterogenousEnergy = 15;

		public SrxmcSimulation(
			MonteCarloWorld world,
			NucleationConfiguration nucleationConfiguration,
			int stepsLimit,
			int hParam,
			int initialNucleonsAmount)
		{
			World = world;
			_addNucleonsOnBordersOnly = nucleationConfiguration.AddNucleonsOnlyOnBorders;
			_nucleationChangeStrategy = nucleationConfiguration.NucleationChangeStrategy;
			_nucleationPropagationType = nucleationConfiguration.NucleationPropagationType;
			_stepsLimit = stepsLimit;
			_hParam = hParam;
			_newNucleonsPerStep = initialNucleonsAmount;
			_allNeighboursStrategy = new MooreNeighbourhoodStrategy();
		}

		public void InitializeCellsEnergy()
		{
			foreach (var monteCarloCell in World.Cells)
				SetCellEnergy(monteCarloCell);
		}
		
		#region SimulationSteps
		public void AddNucleons()
		{
			var nucleonsToAdd = _newNucleonsPerStep;
			var nucleonsAdded = 0;
			var trials = 0;
			var maxTrials = nucleonsToAdd * 2;
			_newNucleonsPerStep = _nucleationChangeStrategy.UpdateNucleation(nucleonsToAdd);

			while (trials < maxTrials)
			{
				trials++;
				var cell = RandomizeCellForNewNucleon();
				if (cell.IsRecrystallized)
					continue;

				var newState = World.HighestCellState + 1;
				cell.StateNumber = newState;
				cell.IsRecrystallized = true;
				SetCellEnergy(cell);
				nucleonsAdded++;
				if (nucleonsAdded >= nucleonsToAdd)
					break;
			}
		}

		public void RecrystallizationStep()
		{
			foreach (var cell in World.Cells.Cast<MonteCarloCell>().OrderBy(c => Guid.NewGuid()))
			{
				var neighbours = _allNeighboursStrategy.GetNeighbours(cell, World).ToList();
				if (!neighbours.Any(n => n.IsRecrystallized))
					continue;

				var hParam = cell.IsRecrystallized ? 0 : _hParam;
				var newState = neighbours.OrderBy(x => Guid.NewGuid()).First(x => x.IsRecrystallized).StateNumber;
				var energyBefore = CalculateEnergy(cell, neighbours) + hParam;
				var energyAfter = CalculateEnergy(cell, neighbours, newState);

				if (energyBefore > energyAfter)
				{
					cell.IsRecrystallized = true;
					cell.Energy = 0;
					cell.StateNumber = newState;
				}
			}
			_step++;
		}

		#endregion

		private void SetCellEnergy(MonteCarloCell cell)
		{
			if (_nucleationPropagationType == NucleationPropagationType.Heterogenous && IsOnBorder(cell, World))
				cell.Energy = InitialCellHeterogenousEnergy;
			else
				cell.Energy = InitialCellEnergy;
		}

		private MonteCarloCell RandomizeCellForNewNucleon()
		{
            return _addNucleonsOnBordersOnly ? World.GetRandomCell(c => IsOnBorder(c, World)) : World.GetRandomCell();
		}

		private bool IsOnBorder(MonteCarloCell cell, World<MonteCarloCell> world)
		{
			var state = cell.StateNumber;
			return _allNeighboursStrategy.GetNeighbours(cell, world).Any(c => c.StateNumber != state);
		}

		private int CalculateEnergy(MonteCarloCell cell, IEnumerable<MonteCarloCell> neighbours, int? forcedState = null)
		{
			var state = forcedState ?? cell.StateNumber;
			return JParam * neighbours.Count(n => n.StateNumber != state);
		}
	}
}
