﻿namespace CellularAutomata.Core
{
	public interface INucleationChangeStrategy
	{
		int UpdateNucleation(int currentNucleonsPerStep);
	}
}