﻿using CellularAutomata.Core.MonteCarloModels;

namespace CellularAutomata.Core
{
	public interface IMonteCarloWorldFactory
	{
		MonteCarloWorld Create(int size);
	}
}