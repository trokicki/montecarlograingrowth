using System;
using System.Linq;
using CellularAutomata.Core.MonteCarloModels;

namespace CellularAutomata.Core.Extensions
{
	public static class MonteCarloWorldExtensions
	{
		private static readonly Random Random;

		static MonteCarloWorldExtensions()
		{
			Random = new Random();
		}

		public static MonteCarloCell GetRandomCell(this MonteCarloWorld world)
		{
			var x = Random.Next(world.Size);
			var y = Random.Next(world.Size);
			return world.Cells[x, y];
		}

		public static MonteCarloCell GetRandomCell(this MonteCarloWorld world, Predicate<MonteCarloCell> predicate)
		{
			var cell = world.Cells.Cast<MonteCarloCell>().OrderBy(x => Guid.NewGuid()).FirstOrDefault(c => predicate(c));
			if(cell == null)
				throw new CellNotFoundException();
			return cell;
		}
	}

	public class CellNotFoundException : Exception { }
}