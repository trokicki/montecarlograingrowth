using System.Collections.Generic;
using CellularAutomata.Core.Models;
using CellularAutomata.Core.MonteCarloModels;

namespace CellularAutomata.Core
{
	public interface INeighbourhoodStrategy
	{
		IEnumerable<MonteCarloCell> GetNeighbours(ICell cell, World<MonteCarloCell> world);
	}
}