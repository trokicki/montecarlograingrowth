﻿using System.Linq;
using CellularAutomata.Core.Models;
using CellularAutomata.Core.MonteCarloModels;

namespace CellularAutomata.Core.GrainGrowth
{
	public class GrainGrower
	{
		protected INeighbourhoodStrategy NeighbourhoodStrategy { get; set; }

		public GrainGrower(INeighbourhoodStrategy neighbourhoodStrategy)
		{
			NeighbourhoodStrategy = neighbourhoodStrategy;
		}

		public bool PerformGrainGrowthStep(World<MonteCarloCell> world)
		{
			foreach (var cell in world.Cells.Cast<MonteCarloCell>().Where(cell => cell.StateNumber > 0))
			{
				foreach (var neighbour in NeighbourhoodStrategy.GetNeighbours(cell, world).Where(n => n.StateNumber == 0))
					neighbour.NextStateNumber = cell.StateNumber;
			}

			var changes = TransformStateNumbers(world);
			return changes > 0;
		}

		private int TransformStateNumbers(World<MonteCarloCell> world)
		{
			var changes = 0;
			foreach (var cell in world.Cells.Cast<MonteCarloCell>().Where(cell => cell.NextStateNumber > 0))
			{
				cell.StateNumber = cell.NextStateNumber;
				cell.NextStateNumber = -1;
				changes++;
			}
			return changes;
		}
	}
}
