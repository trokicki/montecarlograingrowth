﻿using System;

namespace CellularAutomata.Core.NucleationChangeStrategies
{
	public class ConstantNucleationChangeStrategy : INucleationChangeStrategy
	{
		public int UpdateNucleation(int currentNucleonsPerStep)
		{
			return currentNucleonsPerStep;
		}
	}

	public class IncreasingNucleationChangeStrategy : INucleationChangeStrategy
	{
		private readonly int _increasePerStep;

		public IncreasingNucleationChangeStrategy(int increasePerStep)
		{
			if(increasePerStep < 0)
				throw new Exception("increasePerStep must be greater than 0.");
			_increasePerStep = increasePerStep;
		}
		
		public int UpdateNucleation(int currentNucleonsPerStep)
		{
			return currentNucleonsPerStep + _increasePerStep;
		}
	}

	public class DecreasingNucleationChangeStrategy : INucleationChangeStrategy
	{
		private readonly int _decreasePerStep;

		public DecreasingNucleationChangeStrategy(int decreasePerStep)
		{
			_decreasePerStep = decreasePerStep;
		}

		public int UpdateNucleation(int currentNucleonsPerStep)
		{
			return currentNucleonsPerStep - _decreasePerStep;
		}
	}

	public class SingleFeedNucleationChangeStrategy : INucleationChangeStrategy
	{
		public int UpdateNucleation(int currentNucleonsPerStep)
		{
			return 0;
		}
	}
}