using CellularAutomata.Core.Models;
using CellularAutomata.Core.MonteCarloModels;

namespace CellularAutomata.Core
{
	public interface IMonteCarloWorldInitializer
	{
		void InitializeWorld(World<MonteCarloCell> world, double coverage);
	}
}